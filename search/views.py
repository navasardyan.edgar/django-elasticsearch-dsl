from django.shortcuts import render
from search.documents import CarDocument


def search(request):
    q = request.GET.get('q')
    if q:
        cars = CarDocument.search().query("match", name=q)
    else:
        cars = ''

    return render(request, 'search.html', {'cars': cars})
