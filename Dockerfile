FROM python:3
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PIP_TOKEN_NAME=pipinstall
ENV PIP_TOKEN=fNdvDP7RyNvJsWN4uZxu
ENV MTS_WEB_SERVICE_URL=''
ENV HOST_MTS=''

WORKDIR /
RUN apt-get -y update
RUN apt-get upgrade -y
RUN apt-get install -y --fix-missing \
    gcc \
    git \
    wget \
    curl \
    pkg-config \
    python-dev \
    python-pip \
    software-properties-common \
    zip \
    build-essential \
    libaio1 \
    libxml2-dev \
    libxslt-dev \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
RUN python get-pip.py
RUN pip -V
RUN pip install --no-cache-dir wheel
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --upgrade sentry-sdk
RUN pip install ll_integration==1.0.10 --extra-index-url https://${PIP_TOKEN_NAME}:${PIP_TOKEN}@gitlab.loglab.ru/api/v4/projects/38/packages/pypi/simple
RUN pip install ll-wms-connector-api==0.3.0 --extra-index-url https://${PIP_TOKEN_NAME}:${PIP_TOKEN}@gitlab.loglab.ru/api/v4/projects/51/packages/pypi/simple
RUN rm requirements.txt
COPY . /app
WORKDIR /app
EXPOSE 9200
EXPOSE 9200
