from django.contrib import admin
from main.models import Car


class CarAdmin(admin.ModelAdmin):
    pass


admin.site.register(Car, CarAdmin)
